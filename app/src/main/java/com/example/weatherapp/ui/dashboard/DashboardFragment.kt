package com.example.weatherapp.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.ScrollingActivity
import com.example.weatherapp.databinding.FragmentDashboardBinding
import com.example.weatherapp.network.City
import com.example.weatherapp.ui.home.HomeViewModel
import com.example.weatherapp.ui.recview.OnCityEventListener
import com.example.weatherapp.ui.recview.RecyclerAdapter

class DashboardFragment : Fragment() {
    private val dashboardViewModel: DashboardViewModel by activityViewModels()
    private var _binding: FragmentDashboardBinding? = null
    private lateinit var adapter: RecyclerAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashboardViewModel.favouritesList.observe(viewLifecycleOwner){
            binding.rvFavourites.layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )

            adapter = RecyclerAdapter(adapterListener)
            adapter.setPlayers(it.toCollection(arrayListOf()))

            binding.rvFavourites.adapter = adapter
        }
        dashboardViewModel.getFavouriteCities(requireContext())
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private val adapterListener=object : OnCityEventListener {
        override fun onCitySelected(city: City) {
            val intent= Intent(activity, ScrollingActivity::class.java).apply {
                putExtra("city",city)
            }
            startActivity(intent)
            Toast.makeText(activity,"Item is clicked", Toast.LENGTH_LONG).show()
        }

        override fun onImageButtonSelected(isCheked:Boolean, city: City) {
            if (isCheked){
                dashboardViewModel.insertFavouriteCity(requireContext(),city)
            }else{
                dashboardViewModel.deleteFavouriteCity(requireContext(),city)
            }

        }
    }
}