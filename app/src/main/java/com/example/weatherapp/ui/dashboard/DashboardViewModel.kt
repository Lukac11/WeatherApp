package com.example.weatherapp.ui.dashboard

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.database.AppDatabase
import com.example.weatherapp.network.City
import kotlinx.coroutines.launch

class DashboardViewModel : ViewModel() {
    val favouritesList=MutableLiveData<List<City>>()
    fun insertFavouriteCity(context: Context, city: City){
        viewModelScope.launch {
            AppDatabase.getDatabase(context).cityDao().insertCity(city)
        }

    }
    fun deleteFavouriteCity(context: Context, city: City){
        viewModelScope.launch {
            AppDatabase.getDatabase(context).cityDao().delete(city)
        }
    }
    fun getFavouriteCities(context: Context){
        viewModelScope.launch {
            favouritesList.value=AppDatabase.getDatabase(context).cityDao().getAll()
        }
    }
}