package com.example.weatherapp.ui.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.database.AppDatabase
import com.example.weatherapp.database.CityDao
import com.example.weatherapp.network.City
import com.example.weatherapp.network.Location
import com.example.weatherapp.network.Network
import kotlinx.coroutines.launch

class HomeViewModel() : ViewModel() {
    var searchInput=MutableLiveData<String>()
    var inputSearch:String=""
    val cityList=MutableLiveData<ArrayList<City>>()
    val location=MutableLiveData<Location>()
    val recentsList=MutableLiveData<List<City>>()
    fun getCityList(city:String){
        viewModelScope.launch {
            cityList.value=Network().getservice().searchCity(city)
        }
    }
    fun getLocation(id:Int){
        viewModelScope.launch {
            location.value=Network().getservice().getLocation(id)
        }
    }
    fun addInput(input:String){
        inputSearch=input
        viewModelScope.launch {
            searchInput.postValue(inputSearch)
        }
        //searchInput.postValue(inputSearch)
    }
    fun insertFavouriteCity(context: Context, city: City){
        viewModelScope.launch {
            AppDatabase.getDatabase(context).cityDao().insertCity(city)
        }

    }
    fun deleteFavouriteCity(context: Context,city: City){
        viewModelScope.launch {
            AppDatabase.getDatabase(context).cityDao().delete(city)
        }
    }
    fun getRecentCities(context:Context){
        viewModelScope.launch {
            recentsList.value=AppDatabase.getDatabase(context).cityDao().getRecents()
        }

    }

}