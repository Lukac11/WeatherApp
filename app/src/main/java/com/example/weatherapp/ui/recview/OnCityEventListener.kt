package com.example.weatherapp.ui.recview

import com.example.weatherapp.network.City

interface OnCityEventListener {
    fun onCitySelected(city: City)

    fun onImageButtonSelected(isCheked: Boolean, city: City)
}