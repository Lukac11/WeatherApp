package com.example.weatherapp.ui.recview

import android.content.ContentValues.TAG
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.databinding.ItemCityBinding
import com.example.weatherapp.network.City

class RecyclerAdapter(val onCitySelectedListener: OnCityEventListener) : RecyclerView.Adapter<RecyclerAdapter.CityViewHolder>() {
    private var  cities = ArrayList<City>()
    //var onCitySelectedListener: OnCityEventListener? = null

    fun setPlayers(cities: ArrayList<City>) {
        this.cities.clear()
        this.cities.addAll(cities)
        this.notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_city, parent, false)
        return CityViewHolder(view)
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val city = cities[position]
        holder.bind(city)
        onCitySelectedListener?.let { listener ->
            holder.itemView.setOnClickListener {
                listener.onCitySelected(city)
            }
        }


    }

    override fun getItemCount(): Int = cities.count()



inner class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(city: City) {
        val binding = ItemCityBinding.bind(itemView)
        binding.tvCityname.text = city.title
        binding.tvLocationType.text = city.location_type
        binding.tvLatlon.text = city.latt_long.toString()
        binding.tvWoeid.text = city.woeid.toString()
        binding.imgbtnAddtofavourites.setOnClickListener {

            if (binding.imgbtnAddtofavourites.getTag()=="kljuc"){
                binding.imgbtnAddtofavourites.setImageResource(R.drawable.ic_baseline_star_outline_24)
                onCitySelectedListener.onImageButtonSelected(false,city = city)
            }else{
                onCitySelectedListener.onImageButtonSelected(true,city = city)
                binding.imgbtnAddtofavourites.setTag("kljuc")
                binding.imgbtnAddtofavourites.setImageResource(R.drawable.ic_baseline_star_24)
            }

        }


    }

}
}