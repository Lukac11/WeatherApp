package com.example.weatherapp.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.ScrollingActivity
import com.example.weatherapp.databinding.FragmentHomeBinding
import com.example.weatherapp.network.City
import com.example.weatherapp.ui.recview.OnCityEventListener
import com.example.weatherapp.ui.recview.RecyclerAdapter

class HomeFragment : Fragment() {
    private val homeViewModel: HomeViewModel by activityViewModels()
    private var _binding: FragmentHomeBinding? = null
    private lateinit var adapter: RecyclerAdapter
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        homeViewModel.recentsList.observe(viewLifecycleOwner){
            binding.rvCities.layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )

            adapter = RecyclerAdapter(adapterListener)
            adapter.setPlayers(it.toCollection(arrayListOf()))

            binding.rvCities.adapter = adapter

        }
        homeViewModel.getRecentCities(requireContext())
        binding.btnSearch.setOnClickListener {
            homeViewModel.addInput(binding.etSearch.text.toString())
            homeViewModel.cityList.observe(viewLifecycleOwner) {
                binding.rvCities.layoutManager = LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )

                adapter = RecyclerAdapter(adapterListener)
                adapter.setPlayers(it)

                binding.rvCities.adapter = adapter
            }
            homeViewModel.getCityList(binding.etSearch.text.toString())
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }






    private val adapterListener=object : OnCityEventListener{
        override fun onCitySelected(city: City) {
            val intent= Intent(activity, ScrollingActivity::class.java).apply {
                putExtra("city",city)
            }
            startActivity(intent)
            Toast.makeText(activity,"Item is clicked", Toast.LENGTH_LONG).show()
            city.isRecent=true
            homeViewModel.insertFavouriteCity(requireContext(),city)
        }

        override fun onImageButtonSelected(isCheked:Boolean, city: City) {
            if (isCheked){
                city.isFavourite=true
                homeViewModel.insertFavouriteCity(requireContext(),city)

            }else{
                homeViewModel.deleteFavouriteCity(requireContext(),city)
                city.isFavourite=false
            }

        }
    }
}