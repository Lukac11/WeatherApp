package com.example.weatherapp

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.network.ConsolidatedWeather
import com.example.weatherapp.network.Location
import com.example.weatherapp.network.Network
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ScrollingViewModel:ViewModel() {
    val location= MutableLiveData<Location>()
    val consWeather=MutableLiveData<ArrayList<ConsolidatedWeather>>()
    fun getLocation(id:Int){
        viewModelScope.launch (Dispatchers.IO){
            location.postValue(Network().getservice().getLocation(id))
        }
    }
    fun getDailyWeather(id:Int){
        val calendar=Calendar.getInstance()
        val date=calendar.time
        val simpleDateFormat=SimpleDateFormat("yyyy/MM/dd")
        Log.d(TAG,"date "+ simpleDateFormat.format(date))
        viewModelScope.launch (Dispatchers.IO){
            consWeather.postValue(Network().getservice().getDailyWeather(id,simpleDateFormat.format(date)))
        }
    }
}