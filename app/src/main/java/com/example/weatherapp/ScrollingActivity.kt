package com.example.weatherapp

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.weatherapp.databinding.ActivityScrollingBinding
import com.example.weatherapp.network.City
import com.example.weatherapp.ui.home.HomeViewModel

class ScrollingActivity : AppCompatActivity() {
    private val viewModel: ScrollingViewModel by viewModels()

    //private val viewModel = ViewModelProvider(this).get(ScrollingViewModel::class.java)
    private lateinit var binding: ActivityScrollingBinding
    private lateinit var city: City
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScrollingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(findViewById(R.id.toolbar))
        city = getIntent().getSerializableExtra("city") as City
        viewModel.location.observe(this) {
            Log.d(TAG, "onCreate: " + it.consolidatedWeather.size.toString())

            Log.d(TAG, "woeid: " + it.woeid)


        }
        viewModel.consWeather.observe(this) {
            binding.contentLayout.tvLocationMintemp?.text = "Min temp: " + it[0].minTemp
            binding.contentLayout.tvLocationMaxtemp?.text = "Max temp: " + it[0].maxTemp
            binding.contentLayout.tvHumidity?.text = "Humidity: " + it[0].humidity
            binding.contentLayout.tvVisibility?.text = "Visibility: " + it[0].visibility
            binding.contentLayout.tvWind?.text = "Wind " + it[0].windSpeed


        }
        viewModel.getLocation(city.woeid)
        viewModel.getDailyWeather(city.woeid)

        binding.toolbarLayout.title = city.title


    }
}