package com.example.weatherapp.network

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
@Entity
data class City(
    val title:String,
    val location_type:String,
    val latt_long:String,
    @PrimaryKey
    val woeid:Int,
    var isRecent:Boolean,
    var isFavourite:Boolean
):Serializable

/*data class CityResponse(
    val :List<City>
):Serializable*/
data class Location(
    @SerializedName("consolidated_weather" ) var consolidatedWeather : ArrayList<ConsolidatedWeather>,
    @SerializedName("time"                 ) var time                : String?,
    @SerializedName("sun_rise"             ) var sunRise             : String?,
    @SerializedName("sun_set"              ) var sunSet              : String?,
    @SerializedName("timezone_name"        ) var timezoneName        : String?,
    @SerializedName("parent"               ) var parent              : City?,
    @SerializedName("title"                ) var title               : String?,
    @SerializedName("location_type"        ) var locationType        : String?,
    @SerializedName("woeid"                ) var woeid               : Int?,
    @SerializedName("latt_long"            ) var lattLong            : String?,
    @SerializedName("timezone"             ) var timezone            : String?
)
data class ConsolidatedWeather(
    @SerializedName("id"                     ) var id                   : Long?,
    @SerializedName("weather_state_name"     ) var weatherStateName     : String?,
    @SerializedName("weather_state_abbr"     ) var weatherStateAbbr     : String?,
    @SerializedName("wind_direction_compass" ) var windDirectionCompass : String?,
    @SerializedName("created"                ) var created              : String?,
    @SerializedName("applicable_date"        ) var applicableDate       : String?,
    @SerializedName("min_temp"               ) var minTemp              : Double?,
    @SerializedName("max_temp"               ) var maxTemp              : Double?,
    @SerializedName("the_temp"               ) var theTemp              : Double?,
    @SerializedName("wind_speed"             ) var windSpeed            : Double?,
    @SerializedName("wind_direction"         ) var windDirection        : Double?,
    @SerializedName("air_pressure"           ) var airPressure          : Double?,
    @SerializedName("humidity"               ) var humidity             : Int?,
    @SerializedName("visibility"             ) var visibility           : Double?,
    @SerializedName("predictability"         ) var predictability       : Int?

)
