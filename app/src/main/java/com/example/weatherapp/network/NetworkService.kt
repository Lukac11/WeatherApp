package com.example.weatherapp.network


import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkService {
    @GET("location/search/?query=(query)")
    suspend fun searchCity(@Query("query") city:String):ArrayList<City>
    @GET("/api/location/{woeid}/")
    suspend fun getLocation(@Path("woeid")woeid:Int):Location
    @GET("/api/location/{woeid}/{date}/")
    suspend fun getDailyWeather(@Path("woeid")woeid: Int, @Path("date")date:String):ArrayList<ConsolidatedWeather>
}