package com.example.weatherapp.database

import androidx.room.*
import com.example.weatherapp.network.City
import java.util.ArrayList

@Dao
interface CityDao {
    @Query("SELECT * FROM City WHERE isFavourite='1'")
    suspend fun getAll(): List<City>

    @Query("SELECT * FROM City WHERE isRecent = '1'")
    suspend fun getRecents():List<City>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCity(city: City)

    @Delete
    suspend fun delete(city: City)
}